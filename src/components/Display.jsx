import React from "react";
import "./Display.css";

// eslint-disable-next-line import/no-anonymous-default-export
export default (props) => (
  <div data-testid="display_test" className="display">
    {props.value}
  </div>
);
